/** tính tiền lương nhân viên
 *
 * biết lương 1 ngày 100k
 *
 * người dùng nhập vào số ngày
 *
 * xử lý số ngày x cho 100 xuất ra màn hình thông báo lương
 */
function cal() {
  var inputvalue = document.getElementById("input__cal").value;
  var result_cal = inputvalue * 100000;
  var content_cal = `<p>Số tiền lương của bạn là : ${result_cal} vnd</p>`;
  document.getElementById("result__cal").innerHTML = content_cal;
}

/** Tính giá trị trung bình
 *
 * nhập vào 5 số thực
 *
 * xử lý tính giá trị trung bình và xuất ra màn hình
 *
 */
function cal_average() {
  var inputnumber1 = document.getElementById("number1").value;
  var inputnumber2 = document.getElementById("number2").value;
  var inputnumber3 = document.getElementById("number3").value;
  var inputnumber4 = document.getElementById("number4").value;
  var inputnumber5 = document.getElementById("number5").value;
  const resulf_average =
    inputnumber1 / 5 +
    inputnumber2 / 5 +
    inputnumber3 / 5 +
    inputnumber4 / 5 +
    inputnumber5 / 5;
  var content_average = `<p>Giá Trị Trung Bình Của 5 Số thực trên là :${resulf_average}</p>`;
  document.getElementById("result__average").innerHTML = content_average;
}
/** Quy Đổi Tiền
 *
 * biết giá trị của 1usd =23500vnd
 *
 * người dùng nhập vào số tiền usd
 *
 * xử lý tính toán vào xuất ra màn hình giá trị quy đổi vnd
 *
 */
function cal3() {
  var inputvalue3 = document.getElementById("input__cal3").value;
  var result_cal3 = inputvalue3 * 23500;
  var content_cal3 = `<p>Số tiền đã quy đổi của bạn là : ${result_cal3} vnd</p>`;
  document.getElementById("result__cal3").innerHTML = content_cal3;
}
/** Tính Diện Tích Chu Vi hình chữ nhật
 *
 * người dùng nhập vào 2 cạnh hình chữ nhật
 *
 * tính toán chu vi dài + rộng * 2
 * diện tích dài * rộng
 *
 * xuất ra màn hình kết quả
 *
 */
function S_C() {
  var longsvalue = document.getElementById("longs").value;
  var widthcvalue = document.getElementById("widthc").value;
  const C = longsvalue * 2 + widthcvalue * 2;
  const S = longsvalue * widthcvalue;
  var content_SC = `<p>Diện Tích Hình Chữ Nhật là: ${S}</p>
  <p>Chu Vi Hình Chữ Nhật là : ${C} </p>`;
  document.getElementById("SC").innerHTML = content_SC;
}

/** tính tổng 2 ký số
 *
 * người dùng nhập vào 2 ký số
 *
 * tính tổng 2 ký số  xuất ra màn hình tổng 2 ký số
 */
function digit() {
  var signvalue = document.getElementById("sign").value;
  const first = Math.floor(signvalue / 10);
  const last = signvalue % 10;
  const sum = first + last;
  var content_digit = `<p>tổng 2 ký số là: ${sum}</p>`;
  document.getElementById("sum").innerHTML = content_digit;
}
