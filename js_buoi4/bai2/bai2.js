function nextDay(a, b, c) {
  if (a == 31 && b == 12) {
    a = 1;
    b = 1;
    c++;
  } else if (a == 30 && b == 11) {
    a = 1;
    b = 12;
  } else if (a == 31 && b == 10) {
    a = 1;
    b = 11;
  } else if (a == 30 && b == 9) {
    a = 1;
    b = 10;
  } else if (a == 31 && b == 8) {
    a = 1;
    b = 9;
  } else if (a == 31 && b == 7) {
    a = 1;
    b = 8;
  } else if (a == 30 && b == 6) {
    a = 1;
    b = 7;
  } else if (a == 31 && b == 5) {
    a = 1;
    b = 6;
  } else if (a == 30 && b == 4) {
    a = 1;
    b = 5;
  } else if (a == 31 && b == 3) {
    a = 1;
    b = 4;
  } else if (a == 28 && b == 2) {
    a = 1;
    b = 3;
  } else if (a == 31 && b == 1) {
    a = 1;
    b = 2;
  } else {
    a++;
  }
  document.getElementById(
    "result-date"
  ).innerHTML = `<p>ngày mai là ngày:${a} tháng:${b} năm:${c}</p>`;
  return a, b, c;
}
function rewindDay(a, b, c) {
  if (a == 1 && b == 1) {
    a = 31;
    b = 12;
    c--;
  } else if (a == 1 && b == 12) {
    a = 30;
    b = 11;
  } else if (a == 1 && b == 11) {
    a = 31;
    b = 10;
  } else if (a == 1 && b == 10) {
    a = 30;
    b = 9;
  } else if (a == 1 && b == 9) {
    a = 31;
    b = 8;
  } else if (a == 1 && b == 8) {
    a = 31;
    b = 7;
  } else if (a == 1 && b == 7) {
    a = 30;
    b = 6;
  } else if (a == 1 && b == 6) {
    a = 31;
    b = 5;
  } else if (a == 1 && b == 5) {
    a = 30;
    b = 4;
  } else if (a == 1 && b == 4) {
    a = 31;
    b = 3;
  } else if (a == 1 && b == 3) {
    a = 28;
    b = 2;
  } else if (a == 1 && b == 2) {
    a = 31;
    b = 1;
  } else {
    a--;
  }
  document.getElementById(
    "result-date"
  ).innerHTML = `<p>hôm qua là ngày:${a} tháng:${b} năm:${c}</p>`;
  return a, b, c;
}
function rewind() {
  var dayValue = document.getElementById("txt-date").value;
  var monthValue = document.getElementById("txt-month").value;
  var yearValue = document.getElementById("txt-year").value;
  var dayRewind = rewindDay(dayValue, monthValue, yearValue);
}
function next() {
  var dayValue = document.getElementById("txt-date").value;
  var monthValue = document.getElementById("txt-month").value;
  var yearValue = document.getElementById("txt-year").value;
  var dayNext = nextDay(dayValue, monthValue, yearValue);
}
/**
 * Viết chương trình nhập vào tháng, năm. Cho biết tháng đó có bao nhiêu ngày. (bao gồm tháng
của năm nhuận).
 */
function tinhNam(x, y) {
  if (x == 2) {
    if ((y % 4 == 0 && y % 100 != 0) || y % 400 == 0) {
      return "29";
    } else {
      return "28";
    }
  } else if (
    x == 1 ||
    x == 3 ||
    x == 5 ||
    x == 7 ||
    x == 8 ||
    x == 10 ||
    x == 12
  ) {
    return "31";
  } else if (x == 4 || x == 6 || x == 9 || x == 11) {
    return "30";
  } else {
    return "Xin mời nhập đúng tháng ";
  }
}
function allDate() {
  var mValue = document.getElementById("txt-month-2").value;
  var yValue = document.getElementById("txt-year-2").value;
  var soNgay = tinhNam(mValue, yValue);
  document.getElementById(
    "result-date-2"
  ).innerHTML = `<p>Tháng ${mValue} có ${soNgay} ngày</p>`;
}
/**
 * Viết chương trình nhập vào số nguyên có 3 chữ số. In ra cách đọc nó
 */
function docSo(x) {
  if (x == 1) {
    return "một";
  } else if (x == 2) {
    return "hai";
  } else if (x == 3) {
    return "ba";
  } else if (x == 4) {
    return "bốn";
  } else if (x == 5) {
    return "năm";
  } else if (x == 6) {
    return "sáu";
  } else if (x == 7) {
    return "bảy";
  } else if (x == 8) {
    return "tám";
  } else if (x == 9) {
    return "chín";
  } else if (x == 0) {
    return "không";
  }
}
function read() {
  var itergerValue = document.getElementById("txt-iteger").value;
  if (99 < itergerValue && itergerValue < 1000) {
    hangTram = Math.floor(itergerValue / 100);

    hangChuc = Math.floor((itergerValue % 100) / 10);

    hangDonVi = (itergerValue % 100) % 10;

    var hangTramOutPut = docSo(hangTram);
    var hangChucOutPut = docSo(hangChuc);
    var hangDonViOutPut = docSo(hangDonVi);
    if (hangDonViOutPut == "không" && hangChucOutPut == "không") {
      document.getElementById(
        "result-read"
      ).innerHTML = `<p> ${hangTramOutPut} trăm </p>`;
    } else if (hangChucOutPut == "một" && hangDonViOutPut == "không") {
      document.getElementById(
        "result-read"
      ).innerHTML = `<p> ${hangTramOutPut} trăm mười </p>`;
    } else if (hangChucOutPut == "không" && hangDonViOutPut != "không") {
      document.getElementById(
        "result-read"
      ).innerHTML = `<p> ${hangTramOutPut} trăm lẻ ${hangDonViOutPut}</p>`;
    } else if (hangChucOutPut == "một") {
      document.getElementById(
        "result-read"
      ).innerHTML = `<p> ${hangTramOutPut} trăm mười ${hangDonViOutPut} </p>`;
    } else if (hangDonViOutPut == "không") {
      document.getElementById(
        "result-read"
      ).innerHTML = `<p> ${hangTramOutPut} trăm ${hangChucOutPut} mươi </p>`;
    } else {
      document.getElementById(
        "result-read"
      ).innerHTML = `<p> ${hangTramOutPut} trăm ${hangChucOutPut} mươi ${hangDonViOutPut}</p>`;
    }
  } else {
    document.getElementById("result-read").innerHTML = `"xin mời nhập lại số"`;
  }
}
/**
 * Cho biết tên và tọa độ nhà của 3 sinh viên. Cho biết tọa độ của trường đại học. Viết chương
trình in tên sinh viên xa trường nhất.
 */
function tinhDoanThang(x2, x1, y2, y1) {
  d = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
  return d;
}
function timMax(a, b, c) {
  if (a > b && a > c) {
    return a;
  } else if (b > a && b > c) {
    return b;
  } else if (c > a && c > b) {
    return c;
  }
}
function find() {
  var studen1 = document.getElementById("txt-student-1").value;
  var studen2 = document.getElementById("txt-student-2").value;
  var studen3 = document.getElementById("txt-student-3").value;
  var studentX1 = document.getElementById("txt-toa-do-x-1").value * 1;
  var studentY1 = document.getElementById("txt-toa-do-y-1").value * 1;
  var studentX2 = document.getElementById("txt-toa-do-x-2").value * 1;
  var studentY2 = document.getElementById("txt-toa-do-y-2").value * 1;
  var studentX3 = document.getElementById("txt-toa-do-x-3").value * 1;
  var studentY3 = document.getElementById("txt-toa-do-y-3").value * 1;
  var schoolX = document.getElementById("txt-toa-do-x-4").value * 1;
  var schoolY = document.getElementById("txt-toa-do-y-4").value * 1;
  var d1 = tinhDoanThang(schoolX, studentX1, schoolY, studentY1);
  var d2 = tinhDoanThang(schoolX, studentX2, schoolY, studentY2);
  var d3 = tinhDoanThang(schoolX, studentX3, schoolY, studentY3);
  var max1 = timMax(d1, d2, d3);
  console.log("max1: ", max1);
  if (d1 == max1) {
    document.getElementById(
      "result-howlong"
    ).innerHTML = `<p>bạn ${studen1} là sinh viên xa trường nhất</p>`;
  } else if (d2 == max1) {
    document.getElementById(
      "result-howlong"
    ).innerHTML = `<p>bạn ${studen2} là sinh viên xa trường nhất</p>`;
  } else if (d3 == max1) {
    document.getElementById(
      "result-howlong"
    ).innerHTML = `<p>bạn ${studen3} là sinh viên xa trường nhất</p>`;
  }
}
