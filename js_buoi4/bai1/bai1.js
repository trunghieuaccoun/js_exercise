function timMin(a, b, c) {
  if (a < b && a < c) {
    min = a;
  } else if (b < a && b < c) {
    min = b;
  } else if (c < a && c < b) {
    min = c;
  }
  return min;
}
function timMax(a, b, c) {
  if (a > b && a > c) {
    max = a;
  } else if (b > a && b > c) {
    max = b;
  } else if (c > a && c > b) {
    max = c;
  }
  return max;
}
function timMid(a, b, c) {
  if (a < b && b < c) {
    mid = b;
  } else if (a > b && b > c) {
    mid = b;
  } else if (b < a && a < c) {
    mid = a;
  } else if (b > a && a > c) {
    mid = a;
  } else if (a < c && c < b) {
    mid = c;
  } else if (a > c && c > b) {
    mid = c;
  }
  return mid;
}
function sapxep() {
  var num1Value = document.getElementById("txt-1").value * 1;
  var num2Value = document.getElementById("txt-2").value * 1;
  var num3Value = document.getElementById("txt-3").value * 1;
  var min1 = timMin(num1Value, num2Value, num3Value);
  var max1 = timMax(num1Value, num2Value, num3Value);
  var mid1 = timMid(num1Value, num2Value, num3Value);
  document.getElementById(
    "result"
  ).innerHTML = `<p>thứ tự tăng dần của dẫy số là ${min1} < ${mid1} < ${max1} </p>`;
}
/**
 * bài 2 Chương trình "Chào hỏi"
 */

function know(human) {
  switch (human) {
    case "X": {
      return "Người lạ";
    }
    case "B": {
      return "Bố";
    }
    case "M": {
      return "Mẹ";
    }
    case "A": {
      return "Anh Trai";
    }
    case "E": {
      return "Em Gái";
    }
  }
}
function greeting() {
  var selectValue = document.getElementById("selUser").value;
  var select = know(selectValue);
  document.getElementById(
    "result-greeting"
  ).innerHTML = `<p>Xin Chào ${select}</p>`;
}
/**
 * 3. Cho 3 số nguyên. Viết chương trình xuất ra có bao nhiêu số lẻ và bao nhiêu số chẵn.
 */
function timChanLe(x) {
  if (x % 2 == 0) {
    count = 1;
  } else {
    count = 0;
  }
  return count;
}
function chanLe() {
  var integerValue1 = document.getElementById("txt-integer1").value * 1;
  var integerValue2 = document.getElementById("txt-integer2").value * 1;
  var integerValue3 = document.getElementById("txt-integer3").value * 1;
  var soChan =
    timChanLe(integerValue1) +
    timChanLe(integerValue2) +
    timChanLe(integerValue3);
  var soLe = 3 - soChan;
  document.getElementById(
    "result-count"
  ).innerHTML = `<p>Bạn nhập vào có :${soChan} số chẵn
  và :${soLe} số lẻ</p>`;
}
/**
 * Viết chương trình cho nhập 3 cạnh của tam giác. Hãy cho biết đó là tam giác gì?
 */
function xacDinhLaTamGiac(x, y, z) {
  if (x + y > z && x + z > y && y + z > x) {
    if (x == y && y == z) {
      define = "Tam Giác Đều";
    } else if (x == y || x == z || y == z) {
      define = "Tam Giác Cân";
    } else if (
      x * x == y * y + z * z ||
      y * y == z * z + x * x ||
      z * z == y * y + x * x
    ) {
      define = "Tam Giác Vuông";
    } else {
      define = "Tam Giác Khác";
    }
  } else {
    define = "không tạo thành tam giác";
  }
  return define;
}
function tamgiac() {
  var aValue = document.getElementById("txt-tamgiac-a").value * 1;
  var bValue = document.getElementById("txt-tamgiac-b").value * 1;
  var cValue = document.getElementById("txt-tamgiac-c").value * 1;
  var ex1 = xacDinhLaTamGiac(aValue, bValue, cValue);
  document.getElementById("result-tam-giac").innerHTML = `<p>${ex1}</p>`;
}
