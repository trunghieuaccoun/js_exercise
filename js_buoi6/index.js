var show = document.getElementById("result");
var step = 0;
var sum = 0;
while (sum < 10000) {
  sum = sum + step;
  step++;
}
if (sum >= 10000) {
  step--;
  show.innerHTML = `số nguyên dương thỏa mãn điều kiện trên là : ${step}`;
} else {
  show.innerHTML = `số nguyên dương thỏa mãn điều kiện trên là :${step}`;
}
/**
 * 2. Viết chương trình nhập vào 2 số x, n tính tổng: S(n) = x + x^2
+ x^3 + … + x^n (Sử dụng vòng lặp và hàm)
 */
function tongS() {
  var xValue = document.getElementById("txt-value-x").value * 1;
  var nValue = document.getElementById("txt-value-n").value * 1;
  var sumS = 0;
  for (var i = 0; i <= nValue; i++) {
    sumS = sumS + xValue ** i;
  }
  document.getElementById(
    "result-ex-2"
  ).innerHTML = `<p>tổng của S(n)  x,n : ${sumS}</p>`;
}
/**
 * . Nhập vào n. Tính giai thừa 1*2*...n
 */
function factorial() {
  var fValue = document.getElementById("txt-f").value * 1;
  var sumFac = 1;
  if (fValue == 0) {
    sumFac == 1;
    console.log("sumFac: ", sumFac);
  } else {
    for (var j = 1; j <= fValue; j++) {
      sumFac = sumFac * j;
      console.log("sumFac: ", sumFac);
    }
  }
  document.getElementById(
    "result-ex-3"
  ).innerHTML = `<p> giai thừa của ${fValue} : ${sumFac}</p>`;
}
/**
 * Hãy viết chương trình khi click vào button sẽ in ra 10 thẻ div.
Nếu div nào vị trí chẵn thì background màu đỏ và lẻ thì
background màu xanh.
 */
var divChan = document.createElement("div");
divChan.style.width = "80%";
divChan.style.height = "20px";
divChan.style.background = "red";
divChan.style.color = "white";
divChan.innerHTML = "divChan";
var divLe = document.createElement("div");
divLe.style.width = "80%";
divLe.style.height = "20px";
divLe.style.background = "blue";
divLe.style.color = "white";
divLe.innerHTML = "divLe";
function createTag() {
  for (var t = 0; t <= 10; t++) {
    if (t % 2 == 0) {
      document.getElementById("main").appendChild(divChan);
    } else {
      document.getElementById("main").appendChild(divLe);
    }
  }
}
